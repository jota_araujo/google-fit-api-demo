import React, { Component } from "react";

import { StyleSheet, Text, View, TouchableNativeFeedback } from "react-native";

import CircularProgress from "./components/CircularProgress";

import { normalizeValues } from "./util/utils";

import {
  initialize as initializeGoogleFit,
  readSteps
} from "./util/GoogleFitHelper";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "black",
    paddingVertical: 20
  },
  circles: {
    alignItems: "center",
    justifyContent: "center",
    marginBottom: 100
  },
  footer: {
    position: "absolute",
    display: "flex",
    alignItems: "stretch",
    justifyContent: "space-around",
    height: 200,
    bottom: 0,
    left: 30,
    right: 30
  },
  feedback: {
    alignSelf: "center",
    color: "white",
    fontSize: 14
  },
  labelContainer: {
    backgroundColor: "black",
    height: 40,
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-around"
  }
});

const COLORS = ["aqua", "yellow", "#da70d6", "orange"];

export default class Example extends Component {
  constructor(props) {
    super(props);

    this.state = {
      feedback: "",
      stepData: []
    };
  }

  componentWillMount() {
    initializeGoogleFit();
  }

  componentDidMount() {}

  stepsReceived(err, res) {
    if (err) {
      console.log(err);
      this.setState({
        feedback: err.toString()
      });
      return;
    }
    this.setState({
      feedback: 'Sucesso',
      stepData: normalizeValues(res[0].steps)
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.footer}>
          <View style={styles.labelContainer}>
            {this.state.stepData.map((item, index) => {
              return (
                <Text
                  key={item.value}
                  style={{
                    color: COLORS[index],
                    fontWeight: "bold",
                    fontSize: 16,
                  }}
                >
                  {" "}
                  {`${item.day}/${item.month}: ${item.steps}`}{" "}
                </Text>
              );
            })}
          </View>
          <TouchableNativeFeedback
         onPress={() => readSteps(this.stepsReceived.bind(this))}
        background={TouchableNativeFeedback.SelectableBackground()}>
      <View style={{ alignItems: 'center',  justifyContent: 'center', height: 50, backgroundColor: '#6495ED'}}>
        <Text style={{  alignSelf: 'center', fontWeight: "bold", color: 'white', fontSize: 18 }}>CARREGAR HISTÓRICO</Text>
      </View>
    </TouchableNativeFeedback>

          <Text style={styles.feedback}> {this.state.feedback} </Text>
        </View>

        <View style={styles.circles}>
          {this.state.stepData.map((item, index) => {
            return (
              <CircularProgress
                key={index}
                delay={300 + index * 350}
                progress={item.value}
                size={100 + 48 * index}
                color={COLORS[index]}
              />
            );
          })}
        </View>
      </View>
    );
  }
}
