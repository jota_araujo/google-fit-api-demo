import GoogleFit from "react-native-google-fit";

export const readSteps = cb => {
  const options = {
    startDate: "2019-02-28T00:00:17.971Z",
    endDate: new Date().toISOString()
  };

  GoogleFit.getDailyStepCountSamples(options, cb);
};

export const initialize = (stepsCallback) => {
    console.log("********************* Component will mount");

    GoogleFit.onAuthorize(() => {
      console.log(" GOOGLE FIT AUTHED ***************************************************************");
    });

    GoogleFit.onAuthorizeFailure(() => {
      console.log("************************* GOOGLE FIT AUTH FAILED");
    });

    GoogleFit.authorize();
}
