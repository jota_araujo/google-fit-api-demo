// Calculates  a value that that is 1.2x time greater than any value in the sample data
const getCeiling = arr =>
  arr
    .reduce((acc, v) => {
      acc.push(v.value);
      return acc;
    }, [])
    .sort((a, b) => a - b)
    .pop() * 1.2;

export const normalizeValues = arr => {
  const ceil = getCeiling(arr);

  return arr.map(item => {
    const dateArr = item.date.split("-");
    return {
      steps: item.value,
      value:
        item.value === 0 ? 0 : Math.max(0.1, (item.value / ceil).toFixed(2)),
      month: dateArr[1],
      day: dateArr[2]
    };
  });
};
