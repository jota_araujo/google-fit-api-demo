import React, { Component } from 'react';

import { StyleSheet, Text, View } from 'react-native';

import * as Progress from 'react-native-progress';

const styles = StyleSheet.create({
  progress: {

    position: "absolute"
  },
});



export default class CircularProgress extends Component {

  constructor(props) {
    super(props);

    this.myInterval = null

    this.state = {
        animationStarted: false,
      visible : false,
      progress: props.progress,
      animationProgress: 0
    };
  }

  componentDidMount() {
    requestAnimationFrame(() => {
        this.setState({
          visible: true,
        });
        this.animate();
      });
    
  }

  animate() {
    setTimeout(() => {  // TODO: JS animations are slow!  Move this outside of the JS thread
      this.myInterval = setInterval(() => {
        
        const n = this.state.animationProgress + 0.05;

        if (n <= this.state.progress) {
           this.setState({ animationStarted: true, animationProgress: n });
        }else{
          console.log("interval cleared", this.props.color )
          clearInterval(this.myInterval)
        }
       
      }, 50);
    }, this.props.delay || 1000);
  }

  render() {
    return (
          this.state.visible && this.state.animationStarted && <Progress.Circle
            style={styles.progress}
            thickness={16}
            color={this.props.color || "gray"}
            borderWidth={0}
            strokeCap="round"
            size={this.props.size || 100 }
            progress={this.state.animationProgress}
            indeterminate={false}
          />
    );
  }
}