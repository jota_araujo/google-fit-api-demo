### ⚠️ NOTE: (This project is from 2019. There are more recent code samples in other repositories)

## Description

A React Native demo project that reads your pedometer history from the [Google Fit API Android SDK](https://developers.google.com/fit/android/) .

![screenshot](https://bitbucket.org/jota_araujo/google-fit-api-demo/raw/54cf14751e175238fea630767c559f9be635d47e/googlefit.gif)

## How to run

```sh
$ npm i
$ react-native run-android
```

### Status

- WIP

## License

MIT
